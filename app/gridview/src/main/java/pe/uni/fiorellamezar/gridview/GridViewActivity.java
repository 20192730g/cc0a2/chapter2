package pe.uni.fiorellamezar.gridview;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.Toast;

import java.util.ArrayList;

public class GridViewActivity extends AppCompatActivity {
    GridView gridView;
    ArrayList<String> text = new ArrayList<>();
    ArrayList<Integer> image = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_grid_view);

        gridView = findViewById(R.id.grid_view);
        fillArray();

        GridAdapter gridAdapter = new GridAdapter(this, text, image);
        gridView.setAdapter(gridAdapter);

        gridView.setOnItemClickListener((parent, view, position, id) -> Toast.makeText(getApplicationContext(), text.get(position), Toast.LENGTH_LONG).show());
    }

    private void fillArray(){
        text.add("Gato");
        text.add("Leon");
        text.add("Panda");
        text.add("Perro");
        text.add("Tortuga");
        text.add("Zorro");
        text.add("Conejo");
        text.add("Ave");
        text.add("Mapache");

        image.add(R.drawable.gato);
        image.add(R.drawable.leon);
        image.add(R.drawable.panda);
        image.add(R.drawable.perro);
        image.add(R.drawable.tortuga);
        image.add(R.drawable.zorro);
        image.add(R.drawable.conejo);
        image.add(R.drawable.ave);
        image.add(R.drawable.mapache);

    }
}