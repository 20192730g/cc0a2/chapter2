package pe.uni.fiorellamezar.sharedpreference;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

public class SharedPreferenceActivity extends AppCompatActivity {

    EditText editTextName;
    EditText editTextMessage;
    Button button;
    CheckBox checkBox;
    int counter = 0;

    SharedPreferences sharedPreferences;
    String name;
    String message;
    Boolean isChecked;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shared_preference);

        editTextMessage = findViewById(R.id.edit_text_message);
        editTextName = findViewById(R.id.edit_text_name);

        button = findViewById(R.id.button);
        checkBox = findViewById( R.id.check_box);

        button.setOnClickListener(v -> {
            counter++;
            button.setText(String.valueOf(counter));
        });
        retrieveData();
    }

    @Override
    protected void onPause() {
        super.onPause();
        saveData();
    }

    private void saveData(){
        sharedPreferences = getSharedPreferences("saveData", Context.MODE_PRIVATE);
        name = editTextName.getText().toString();
        message = editTextMessage.getText().toString();
        isChecked = checkBox.isChecked();

        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("key name", name);
        editor.putString("key message", message);
        editor.putInt("key counter", counter);
        editor.putBoolean("key remember", isChecked);
        editor.apply();

        Toast.makeText(getApplicationContext(), "Tus datos están guardados", Toast.LENGTH_LONG).show();
    }

    private void retrieveData(){
        //Tiene que ser el mismo nombre porque con ese nombre se han guardado los datos
        sharedPreferences = getSharedPreferences("saveData", Context.MODE_PRIVATE);
        name = sharedPreferences.getString("key name", null);
        message = sharedPreferences.getString("key message", null);
        counter = sharedPreferences.getInt("key counter", 0);
        isChecked = sharedPreferences.getBoolean("key remember", false);

        editTextName.setText(name);
        editTextMessage.setText(message);
        button.setText(String.valueOf(counter));
        checkBox.setChecked(isChecked);
    }

}