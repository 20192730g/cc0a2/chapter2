package pe.uni.fiorellamezar.spinner;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;

public class SpinnerActivity extends AppCompatActivity {
    ImageView imageViewLogo;
    Spinner spinnerLogo;

    ArrayAdapter<CharSequence> adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_spinner);

        imageViewLogo = findViewById(R.id.image_view_logo);
        spinnerLogo = findViewById(R.id.spinner);
        //Contexto: la clase en la que estamos
        adapter = ArrayAdapter.createFromResource(this, R.array.logos, android.R.layout.simple_spinner_item);
        //el adapter es el intermediario entre el componente y objeto
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spinnerLogo.setAdapter(adapter);
        spinnerLogo.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(position == 0){
                    imageViewLogo.setImageResource(R.drawable.a);
                    return;
                }
                if(position == 2){
                    imageViewLogo.setImageResource(R.drawable.c);
                    return;
                }
                if(position == 1){
                    imageViewLogo.setImageResource(R.drawable.b);
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }
}