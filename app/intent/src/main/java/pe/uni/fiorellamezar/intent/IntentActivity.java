package pe.uni.fiorellamezar.intent;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class IntentActivity extends AppCompatActivity {
    Button button;
    EditText editText, editNumber;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intent);

        editText = findViewById(R.id.edit_text);
        editNumber = findViewById(R.id.edit_number);
        button = findViewById(R.id.button);

        button.setOnClickListener(v -> {
            //obtenemos los valores de los editText
            String sText = editText.getText().toString();
            String sNumber = editNumber.getText().toString();

            //Se instancia un intent que puede ser obtenido en distintas clases
            Intent intent = new Intent(IntentActivity.this, SecondActivity.class); //Asociación de las activities

            intent.putExtra("TEXT", sText); // (nombre en el activity destino, valor que se pasará)

            if (!sNumber.equals("")){
                int number = Integer.parseInt(sNumber);
                intent.putExtra("NUMBER", number);
            }

            startActivity(intent);
            //finish(); //se debe quitar para que al ir a otro activity, cuando se quiera regresar no termine
        });
    }
}