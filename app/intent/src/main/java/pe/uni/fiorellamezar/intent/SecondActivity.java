package pe.uni.fiorellamezar.intent;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

public class SecondActivity extends AppCompatActivity {

    TextView textViewText, textViewNumber;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        textViewText = findViewById(R.id.text_view_text);
        textViewNumber = findViewById(R.id.text_view_number);

        //Este intent se obtiene de la aplicación porque ya se usó un intent
        Intent intent = getIntent();
        
        //Obtenemos los datos
        String text = intent.getStringExtra("TEXT");
        int number = intent.getIntExtra("NUMBER", 0);

        textViewText.setText(text);
        textViewNumber.setText(String.valueOf(number));

    }
}