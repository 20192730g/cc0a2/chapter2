package pe.uni.fiorellamezar.googlecodescanner;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCanceledListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.mlkit.common.MlKitException;
import com.google.mlkit.vision.barcode.common.Barcode;
import com.google.mlkit.vision.codescanner.GmsBarcodeScanner;
import com.google.mlkit.vision.codescanner.GmsBarcodeScannerOptions;
import com.google.mlkit.vision.codescanner.GmsBarcodeScanning;


public class GoogleCodeScannerActivity extends AppCompatActivity {
    TextView textViewScanner;
    Button buttonScanner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_google_code_scanner);

        textViewScanner = findViewById(R.id.text_view_scanner);
        buttonScanner = findViewById(R.id.button_scanner);

        buttonScanner.setOnClickListener(v -> {
            GmsBarcodeScannerOptions options = new GmsBarcodeScannerOptions.Builder()
                    .setBarcodeFormats(Barcode.FORMAT_QR_CODE)
                    .build();
            GmsBarcodeScanner scanner = GmsBarcodeScanning.getClient(getApplicationContext(), options);
            scanner.startScan()
                    .addOnSuccessListener(barcode -> textViewScanner.setText( getSucessfulMessage(barcode)))
                    .addOnCanceledListener(() -> textViewScanner.setText(R.string.cancelled_option))
                    .addOnFailureListener(e -> textViewScanner.setText(getFailureExceptionMessage((MlKitException) e)));
        });
    }

    private String getSucessfulMessage(Barcode barcode){
        return String.format(
                getResources().getString(R.string.barcode_result),
                barcode.getRawValue(),
                barcode.getFormat(),
                barcode.getValueType());

    }

    private String getFailureExceptionMessage(MlKitException e){
        switch(e.getErrorCode()){
            case MlKitException.CODE_SCANNER_CANCELLED:
                return getString(R.string.error_code_scanner_cancelled);
            case MlKitException.UNKNOWN:
                return getString(R.string.error_unknown);
            default:
                return getString(R.string.error_default, e);
        }
    }

}